// perf.js

import * as d3 from 'd3';

// performance measures:
// https://developer.mozilla.org/en-US/docs/Web/API/Performance/measure
// to see all:     performance.getEntriesByType("measure")
// window.performance.getEntries().filter(d => /^data.js.*listener/.test(d.name))

// track open timings
let outstanding = new Map();

const engFormatNumber = d => Intl.NumberFormat(undefined, { notation: "engineering" }).format(d).replace(/E0$/, "").replace(/E/, "e");
const formatNumber = d => d > 1 ? Intl.NumberFormat().format(d) : engFormatNumber(d);

// these are the defaults, 'as shipped', for Perf.  many hard-coded
// parameters, both html and svg (say), could easily be moved into here.
const installedDefaultOptions = {
    html: {
        place: "#perf-display",
        "display-class": "perf-display",
        "buttons-class": "perf-buttons",
        "buttons-above-display": true,
        buttons: [  { name: "generate",
                      classes: ["generate"],
                      addClasses: ["btn", "btn-light"],
                      text: "generate chart"
                    },
                    { name: "clear",
                      classes: ["clear"],
                      addClasses: ["btn", "btn-light"],
                      text: "clear counters"
                    },
                    { name: "remove",
                      classes: ["remove"],
                      addClasses: ["btn", "btn-light"],
                      text: "remove chart"
                    }]
    },
    svg: {
        aspectRatio: 0.6,       // height/width
        minimumWidth: 300       // for when the screen is initially very shrunk
    }
};
// a new instance inherits these.  these can be modified by
// Perf.defaultOptions
let defaultOptions = installedDefaultOptions;

export default class {
    // CLIENTNAME is an arbitrary name for the user of *this* class
    // instance.  all classes share the same graphic, etc.
    //
    // by default, Perf attaches its elements after a DOM node with
    // 'id="perf-display"'.  so, the *default* value of ARGPLACE is
    // "#perf-display" (NB: notice the "#").  but, if *any* instance
    // is constructed with a non-null/undefined ARGPLACE, that is
    // where the graph (for all instances) will show up
    //
    // three buttons are instantiated:
    // - generate :: this generates a graph, using whatever data is in the database
    // - clear :: clears the database
    // - remove :: removes the graph from the DOM

    constructor(clientName, options) {
        const self = this;      // for event routines
        
        this.clientName = clientName;
        this.options = options ?
            merge(defaultOptions, options, true) :
            defaultOptions;
        this.place = d3.select(this.options.html.place); // try to find where
        this.displayDivName = this.options.html["display-class"];
        this.displayDivClass = `.${this.displayDivName}`;
        this.buttonsDivName = this.options.html["buttons-class"];
        this.buttonsDivClass = `.${this.buttonsDivName}`;

        if (this.place) {       // this is how user can enable/disable
            const addDisplay = () =>  {
                  if (!this.place.select(this.displayDivClass).node()) {
                      this.place.append("div")
                          .attr("class", this.displayDivName);
                  }
            };
            const addButtons = () => {
                  if (!this.place.select(this.buttonsDivClass).node()) {
                      this.place.append("div")
                          .attr("class", this.buttonsDivName)
                  }
            };
            if (this.options.html["buttons-above-display"]) {
                addButtons();
                addDisplay();
            } else {
                addDisplay();
                addButtons();
            }
            this.buttonsSelect = this.place.select(this.buttonsDivClass);
            this.displaySelect = this.place.select(this.displayDivClass);
            // now, create (if necessary) our buttons
            this.buttonsSelect.selectAll("button")
                .data(this.options.html.buttons)
                .join("button")
                .text(d => d.text)
                .attr("class", d => `${d.classes.concat(d.addClasses).join(" ")}`)
                .on("click", (e,d) => {
                    // figure out what to do, and do it! (at run time, maalesef)
                    if (d.name === "generate") {
                        self.display();
                    } else if (d.name === "clear") {
                        self.clear();
                    } else if (d.name === "remove") {
                        self.remove();
                    } else {
                        throw new Error(`unknown name "${d.name}" in Perf options.html.buttons[].name`)
                    }
                })
        } else {
            console.log(`Perf: could not find $("${place}") in the DOM.  disabling display functions.`);
        }
    }

    // generate a name for this
    _name (process) {
        return `(perf.js) ${this.clientName}:${process}`;
    }

    // invert _name()
    _unName(name, leaveModule) {
        return leaveModule? 
            name.replace(/^\(perf.js\) +/, "") :
            name.replace(/^\(perf.js\) [^:]*: */, "")


        if (leaveModule) {
            und = `${this.clientName}:${und}`;
        }
        return und;
    }

    // PROCESS in MODULE (just a two-level naming hierarchy) has begun
    begin(process) {
        const name = this._name(process),
              count = outstanding.get(name);
        window.performance.mark(`${name}: begin`);
        outstanding.set(name, count? count+1 : 1);
    }

    // PROCESS in MODULE has finished
    end(process) {
        const name = this._name(process),
              count = outstanding.get(name);

        window.performance.mark(`${name}: end`);
        // and, create a measure
        window.performance.measure(name, `${name}: begin`, `${name}: end`);

        if (!count) {     // that's a problem
            alert("oops, no outstanding, perf.js");
        } else if (count == 1) {
            outstanding.delete(name);
        } else {
            outstanding.set(name, count-1);
        }
    }

    // note TEXT at this point; PROCESS is optional
    mark(process, text) {
        window.performance.mark(`${this_name(process)}: mark ${text}`);
    }

    ourMarks() {
        return window.performance
            .getEntriesByType("mark")
            .filter(d => { return /^\(perf.js\)/.test(d.name) });
    }

    ourMeasures() {
        return window.performance
            .getEntriesByType("measure")
            .filter(d => { return /^\(perf.js\)/.test(d.name) });
    }

    clear() {
        // clear all of our marks/measures
        this.ourMarks().forEach(d => window.performance.clearMarks(d.name));
        this.ourMeasures().forEach(d => window.performance.clearMeasures(d.name));
    }

    outstanding() {
        return [...outstanding.keys()].map(d => this._unName(d, true));
    }

    // build and display a report
    // https://observablehq.com/@d3/horizontal-bar-chart
    display() {
        // get window geometry
        const wWidth = window.innerWidth,
              wHeight = window.innerHeight;
        const margin = {top: 20, right: 20, bottom: 30, left: 75};
        // for use in event handlers.  (but, also, firefox, at least,
        // seems to revert 'this' to Window in arrow functions in,
        // e.g., d3 chains.)
        const self = this;

        // svg, mouse events, etc.:
        // https://jarrettmeyer.com/2018/06/21/d3-capturing-mouse-events
        // https://chartio.com/resources/tutorials/how-to-show-data-on-mouseover-in-d3js/
        // https://www.smashingmagazine.com/2018/05/svg-interaction-pointer-events-property/

        if (!this.place) {
            // no html support defined in .html file
            throw new Error(`Perf: no element "${this.place}" in html file/DOM; perf.display() not available`);
            // NOTREACHED
        }

        const mouseOver = (() => {
            return (function(e,d) {
                const pointer = d3.pointer(e);
                if (d instanceof PerformanceMeasure) {
                    self.svg.selectAll("rect")
                        .attr("opacity", .1)
                    self.ttdiv
                        .attr("transform", `translate(${pointer[0]},${pointer[1]})`)
                        .attr("display", null)
                        .raise();
                    self.ttdiv
                        .select("text")
                        .text(`${self._unName(d.name, true)}: duration ${formatNumber(d.duration)} [${formatNumber(d.startTime)}, ${formatNumber(d.startTime+d.duration)}]`);
                }
            })
        })();
        const mouseOut = (() => {
            return (function(e,d) {
                // filter, to keep from switching back and forth when passing over tooltip text
                if (d instanceof PerformanceMeasure) {
                    self.svg.selectAll("rect")
                        .attr("opacity", 1);
                    self.ttdiv.attr("display", "none");
                }
            })
        })();


        if (this.outstanding().length) {
            console.log("Perf: still some outstanding processes", this.outstanding());
            alert(`Perf: still some outstanding processes: ${this.outstanding()}`);
        }

        this.width = this.displaySelect.node().offsetWidth;
        this.height = this.width*this.options.svg.aspectRatio; // fairly random
        if (this.height >= wHeight/2) {         // too big
            this.height = wHeight/2;
            this.width = this.height/this.options.svg.aspectRatio;
        }
        if (this.width < this.options.svg.minimumWidth) {
            this.width = this.options.svg.minimumWidth;
            this.height = this.width*this.options.svg.aspectRatio;
        }

        this.measures = this.ourMeasures();
        let maxNameLength = d3.max([0, ...this.measures.map(d => d.name.length)]);

        // https://developer.mozilla.org/en-US/docs/Web/CSS/Viewport_concepts
        if (!this.svg) {
            this.svg = this.svg? this.svg : d3.create("svg")
                .attr("viewBox", [0, 0, this.width, this.height])
                .attr("width", "100%")
                .attr("height", this.height);
            this.rects = this.svg.append("g")
                .attr("fill", "none")
                .selectAll("rect");
            this.gx = this.svg.append("g");

            this.gy = this.svg.append("g");

            // https://jarrettmeyer.com/2018/06/21/d3-capturing-mouse-events
            // https://chartio.com/resources/tutorials/how-to-show-data-on-mouseover-in-d3js/
            // https://www.smashingmagazine.com/2018/05/svg-interaction-pointer-events-property/
            // define a div for the tooltip.  we do this *after* the
            // above, so that our tooltip display *above* the bars, etc.
            // https://stackoverflow.com/a/17133220/1527747
            this.ttdiv = this.svg.append("g")
                .attr("y", -8)
                .attr("class", "mytooltip")
                .attr("pointer-events", "none") // keep mouse events from occuring here...
                .attr("display", "none");
            this.ttdiv.append("text")
                .text("");
        }

        this.x = d3.scaleLinear()
            .domain([d3.min(this.measures,
                            d => d.startTime),
                     d3.max(this.measures,
                            d => d.startTime + d.duration)])
            .range([margin.left + (maxNameLength*5), this.width - margin.right]);

        this.y = d3.scaleBand()
            .domain(this.measures.map(d => d.name))
            .range([this.height - margin.bottom, margin.top]);

        this.xAxis = g => g
            .attr("transform", `translate(0,${this.height - margin.bottom})`)
            .call(d3.axisBottom(this.x)
                  .ticks(this.width / 80)
                  .tickSizeOuter(0));

        this.yAxis = g => g
            .attr("transform", `translate(${margin.left+(maxNameLength*5)},0)`)
            .call(d3.axisLeft(this.y)
                  .tickFormat(tick => self._unName(tick, true)))
            .call(g => g.select(".domain").remove());

        // set up axes with correct data
        this.gx.call(this.xAxis);
        this.gy.call(this.yAxis);

        this.rects = this.rects
            .data(this.measures)
            .join("rect")
            .attr("x", d => self.x(d.startTime))
            .attr("width", d => self.x(d.startTime+d.duration) - self.x(d.startTime))
            .attr("y", d => self.y(d.name))
            .attr("height", () => self.y.bandwidth())
            .attr("fill", "steelblue")
            .attr("stroke", "black")
            .on("mouseover", mouseOver)
            .on("mouseout", mouseOut);

        this.place.select(this.displayDivClass).node().appendChild(this.svg.node());
    }

    remove() {
        if (this.place) {
            this.displaySelect.selectAll(`svg`).remove();
        }
    }

    // get (if OPTIONS is 'falsy') or set the default options used by
    // succeeding instances
    static defaultOptions(options) {
        if (!options) {
            return defaultOptions;
        } else {
            defaultOptions = options;
        }
    }

    installedDefaultOptions() {
        return installedDefaultOptions;
    }
}

// utility functions

// simple, recursive, clone of an object
function clone(o) {
    if (typeof o != 'object') {
        return o;
    } else {
        let n = {};
        for (let p of Object.keys(o)) {
            n[p] = clone(o[p]);
        }
        return n;
    }
}

// simple recursive merge of o2 into o1.  a *copy* of o1 is returned;
// if NOEXTENSION, tags in O2 must already exist in O1 or an error is signaled
function merge(o1, o2, noextension) {
    let n = clone(o1),
        o1Keys = Object.keys(o1);

    if (typeof o1 != "object" || typeof o2 != "object") {
        // our simple merrge only does types
        throw new Error("Perf: merge() called with non-object argument");
        // NOtREACHED
    }
    for (let p of Object.keys(o2)) {
        if (noextension && n[p] == undefined) {
            throw new Error(`merge, noextension: property "${p}" in source, not in destnation`);
            // NOTREACHED
        } else if (typeof o2[p] != "object") {
            n[p] = o2[p];
        } else {
            n[p] = merge(n[p], o2[p]);
        }
    }
    return n;
}
