ORGFILE=perf.org
PKGJSON=package.json
NPM=npm

# *we* need staging/{tangled,public} (for our tangle target).
# *staging/makefile may have other directories to produce.
SKELDIRS= staging staging/tangled staging/public public

all: skeldirs tangle staging

.PHONY : staging
staging:
	cd staging && ${MAKE} --no-print-directory

skeldirs:
	@for dir in ${SKELDIRS}; do \
		if [ ! -e $${dir} ]; then mkdir -p $${dir}; fi \
	done

tangle: ${ORGFILE}
	./dotangle.el ${ORGFILE}
	@touch tangle				# try to reduce the number of times we do this...

npminstall: skeldirs
	cd staging && ${MAKE} --no-print-directory npminstall

# reveal ourselves to the world
npmpublishmajor: gitgoodtogo bumpmajor
	${NPM} publish --access public
npmpublishminor: gitgoodtogo bumpminor
	${NPM} publish --access public
npmpublishpatch: gitgoodtogo bumppatch
	${NPM} publish --access public

backup:							# make a copy of ${PKGJSON}
	cp -p ${PKGJSON} ${PKGJSON}.backup

# constant bits of the jq expression
JQTONUM='.|.version=([[(.version/".")|.[]|tonumber]|'
JQTOSTR='|.[]|tostring]|.[0]+"."+.[1]+"."+.[2])'
# modify version number in various ways
bumpmajor: backup
	jq ${JQTONUM}'[.[0]+1, 0, 0]'${JQTOSTR} < ${PKGJSON}.backup > ${PKGJSON}
bumpminor: backup
	jq ${JQTONUM}'[.[0], .[1]+1, 0]'${JQTOSTR} < ${PKGJSON}.backup > ${PKGJSON}
bumppatch: backup
	jq ${JQTONUM}'[.[0], .[1], .[2]+1]'${JQTOSTR} < ${PKGJSON}.backup > ${PKGJSON}

# check git
gitcleanp:
	@git status --short | awk '$$1 !~ /[?][?]/ { x++ } END {exit x}' || \
			(echo "ERROR: git working tree not clean" > /dev/stderr; exit 1)

# https://gist.github.com/justintv/168835#gistcomment-3012111
gitmasterp:
	@[[ `git symbolic-ref --short HEAD` = "master" ]] || \
			(echo "ERROR: git not on branch master" > /dev/stderr; exit 1)

gitgoodtogo: gitcleanp gitmasterp

